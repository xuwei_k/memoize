package scalaz

/**
  * @see [[https://github.com/paf31/purescript-memoize/blob/v2.0.0/src/Data/Function/Memoize.purs]]
  */
trait Tabulate[A] {
  def tabulate[R](f: A => R): A => Need[R]
}

object Tabulate {
  def apply[A](implicit A: Tabulate[A]): Tabulate[A] = A

  def memoize[A, B](f: A => B)(implicit A: Tabulate[A]): A => B =
    a => A.tabulate(f)(a).value

  implicit val int: Tabulate[Int] =
    new Tabulate[Int] {

      def tabulate[R](f: Int => R) = { a: Int =>
        def build(n: Int): NatTrie[R] =
          NatTrie(
            Need(f(n)),
            Need(build(n * 2)),
            Need(build((n * 2) + 1))
          )

        def bits(x: Int): List[Boolean] = {
          @annotation.tailrec
          def loop(acc: List[Boolean], n: Int): List[Boolean] = n match {
            case 1 =>
              acc
            case _ =>
              loop((n % 2 != 0) :: acc, n / 2)
          }
          loop(Nil, x)
        }

        def walk[A](xs: List[Boolean], n: NatTrie[A]): Need[A] = {
          xs match {
            case Nil =>
              n.a
            case false :: bs =>
              Bind[Need].bind(n.left)(walk(bs, _))
            case true :: bs =>
              Bind[Need].bind(n.right)(walk(bs, _))
          }
        }

        a match {
          case 0 => Need(f(0))
          case _ =>
            walk(
              bits(if(a > 0) a else -a),
              if (a > 0) build(1) else build(-1)
            )
        }
      }
    }

}
