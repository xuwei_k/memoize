package scalaz

final case class NatTrie[A](
  a: Need[A],
  left: Need[NatTrie[A]],
  right: Need[NatTrie[A]]
)
