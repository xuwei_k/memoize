package scalaz

object Main {
  def main (args: Array[String]): Unit = {
    val n = 35
    time(fibonacci0(n))
    time(fibonacci2(n))
    time(fibonacci3(n))
  }

  def fibonacci0(n: Int): Int = n match {
    case 0 => 0
    case 1 => 1
    case _ =>
      fibonacci0(n - 1) + fibonacci0(n - 2)
  }

  val fibonacci1: Int => Int = {
    case 0 => 0
    case 1 => 1
    case n =>
      fibonacci2(n - 1) + fibonacci2(n - 2)
  }

  val fibonacci2: Int => Int = Tabulate.memoize{
    fibonacci1
  }

  def fibonacci3(x: Int): Int = {
    @annotation.tailrec
    def go(n: Int, m: Int, i: Int): Int =
      if(i == 0) {
        n
      } else {
        go(m, n + m, i - 1)
      }
    go(0, 1, x)
  }

  def time[A](f: => A): Unit = {
    val start = System.currentTimeMillis
    val r = f
    println(r + " " + (System.currentTimeMillis - start))
  }
}